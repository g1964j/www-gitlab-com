- name: Sec - Section CMAU - Reported Sum of Sec Section SMAU
  base_path: "/handbook/product/performance-indicators/"
  definition: A sum of all SMAUs across all stages in the Sec Section (Secure, Protect) for All users and Paid users.
  target: 10% higher than current month (SaaS and self-managed combined)
  org: Sec Section
  section: Sec
  public: true
  pi_type: Section CMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Currently instrumented to meet OKRs (see instrumentation, Lessons Learned for more details)
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free-to-OSS/education Gold/Ultimate users and paid Gold/Ultimate users
    - Threat Insights data is not yet included in this count until we support user-level metrics in Snowplow for self-managed
  lessons:
    learned:
    - Most groups across the section saw steady performance or moderate growth in July. 
    - Sec metrics collection could be partially impacted or entirely disabled for a quarter or longer to prioritize key scalability efforts. 
    - Quality and reliability of metrics data continues to present challenges in supporting PM analysis and decision-making. 
  monthly_focus:
    goals:
      - Saas-first efforts, including attention to scalability, reliability, performance, and technical debt issues.
      - Work with Data Analytics team and engineering to formulate a plan for re-implementation of metrics collection (assuming metrics are impacted as noted above). 
      - Identify a way to denote underreporting on Sec-related *MAU charts to remove confusion / clarify metrics interpretation
      - Work with the Data Analytics team on improving Self-Managed Uplift for Sec
  metric_name: user_unique_users_all_secure_scanners
  sisense_data:
    chart: 9982146
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10224291
    dashboard: 758607
    embed: v2
- name: Sec, Secure - Stage MAU, SMAU - Unique users who have used a Secure scanner
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of unique users who have run one or more Secure scanners.
  target: 10% higher than current month (SaaS and self-managed combined)
  org: Sec Section
  section: sec
  stage: secure
  public: true
  pi_type: Section MAU, SMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - currently instrumented to meet OKRs (see instrumentation, Lessons Learned for more details)
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free-to-OSS/education Gold/Ultimate users and paid Gold/Ultimate users
    - Threat Insights data is not yet included in this count until we support user-level metrics in Snowplow for self-managed
  lessons:
    learned:
    - Most groups across the section saw steady performance or moderate growth in July. 
    - Sec metrics collection could be partially impacted or entirely disabled for a quarter or longer to prioritize key scalability efforts. 
    - Quality and reliability of metrics data continues to present challenges in supporting PM analysis and decision-making. 
  monthly_focus:
    goals:
      - Saas-first efforts, including attention to scalability, reliability, performance, and technical debt issues.
      - Work with Data Analytics team and engineering to formulate a plan for re-implementation of metrics collection (assuming metrics are impacted as noted above). 
      - Identify a way to denote underreporting on Sec-related *MAU charts to remove confusion / clarify metrics interpretation
      - Work with the Data Analytics team on improving Self-Managed Uplift for Sec
  metric_name: user_unique_users_all_secure_scanners
  sisense_data:
    chart: 9982579
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9984584
    dashboard: 758607
    embed: v2

- name: Secure:Static Analysis - GMAU - Users running Static Analysis jobs
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The highest of the number of unique users who have run SAST or Secret Detection jobs.
  target: Progressively increasing month-over-month, >10%
  org: Sec Section
  section: sec
  stage: secure
  group: static_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track, continued healthy growth of static analysis usage. 
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free OSS Ultimate users and paid Ultimate users.
  lessons:
    learned:
      - Continued healthy growth despite known general data quality issues and missing PI data. 
      - Tracking upgrades is really difficult to today, but we're [starting to get directional data](https://app.periscopedata.com/app/gitlab/718481/Static-Analysis-Metrics---@tmccaslin?widget=11735956&udv=1090066) to better understand how many and what vectors upgrades from SAST come from. I believe that our in-product upgrade paths are now starting to see conversion. Additional ideas are being pursued to continue this trend.
  monthly_focus:
    goals:
      - Scaling and Performance Improvements. [Internal tracking issue](https://gitlab.com/gitlab-org/gitlab/-/issues/337248). 
      - Moving forward we are focused strongly on data accuracy and reduction of false positives. We are actively rolling out a [new vulnerability tracking improvement from Vulnerability Research](https://gitlab.com/groups/gitlab-org/-/epics/5144) which should materially reduce false positives. This approach is appearing to work out and produce more accurate tracking. This also opens a new metrics problem that we need a way to track accuracy with real-world data. We intend to measure the improvements of this change via the [new vulnerability coverage data](https://app.periscopedata.com/app/gitlab/863712/Vulnerability-Info-Coverage) in Sisence to ensure this change results in material positive impact before rolling it out. This feature has gotten heavily delayed due to rapid action DB work and upstream bugs with vulnerability reports that our group does not directly control. 
      - We're working to [improve benchmarking capabilities](https://gitlab.com/gitlab-org/gitlab/-/issues/333054) to be better equipped to compare security results between versions of our analyzers and other third party tools to help customers and prospects answer questions about the depth and breadth of what types of vulns can we detect. 
      - To increase efficiency and reduce maintenance costs of our 10 linter analyzers we are [transitioning to SemGrep for many of our linter analyzers](https://gitlab.com/groups/gitlab-org/-/epics/5245). If successful we will reduce ~10 open source analyzers into 1 while also improving the detection rules and proving a more consistent rule management experience across these analyzers, this will be materially impactful as we seek to build our proprietary SAST engine on top of these results. [In 13.12 we released this capability](https://about.gitlab.com/releases/2021/05/22/gitlab-13-12-released/#semgrep-sast-analyzer-for-javascript-typescript-and-python) for JS, TS, and Python and are pursuing additional language transitions via [our GSoC project](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/gitlab-gsoc-2021/-/issues/3). We're already pushing 550k Semgrep jobs with ~10k more per week! ~10% growth from last report. 
  metric_name: xMAU, static analysis - paid GMAU and all GMAU
  sisense_data:
    chart: 9980130
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9967764
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Static Analysis - Job Growth
  base_path: "/handbook/product/performance-indicators/"
  definition: Static analysis job growth is an indicator of expanded deployment of static analysis across customer repositories. This growth is distinct from GMAU which will not increase as customers expand their rollout of secure across projects. 
  target: Progressively increasing month-over-month, >10%
  org: Product
  section: sec
  stage: secure
  group: static_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: false
  is_key: false
  health:
    level: 3
    reasons:
    - On track, continued steady performance of static analysis usage. 
  implementation:
    status: Complete
  lessons:
    learned:
      - Small decline is likely due to missing self managed data due to known data ingest issue that was investigated by PI team and considered resolved. 
      - Job Performance and failures is a key indicator of SAST analyzer health. The [new job performance and status dashboard](https://app.periscopedata.com/app/gitlab/833722/Static-Analysis-Analyzer-job-performance ) provides us an unprecedented understanding of average performance and failure trends for each individual analyzer. We also are seeing [continued improvements in successful job rates](https://app.periscopedata.com/app/gitlab/718481/Static-Analysis-Metrics---@tmccaslin?widget=10143599&udv=1090066). We also have added a new [artifact size by analyzer graph](https://app.periscopedata.com/app/gitlab/833722/Static-Analysis-Analyzer-job-performance?widget=12404496&udv=0). Huge shoutout to Thomas Woodham from his work on this dashboard. 
  monthly_focus:
    goals:
      - All growth experiments and features to expand access to Static Analysis features are on hold for performance and scaling improvements work. [Internal tracking issue](https://gitlab.com/gitlab-org/gitlab/-/issues/337248). 
  metric_name: user_sast_jobs, user_secret_detection_jobs
  sisense_data:
    chart: 9381795
    dashboard: 718481
    embed: v2
  sisense_data_secondary:
    chart: 9430191
    dashboard: 718481
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Static Analysis - CatMAU - Users running Code Quality
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: Unique users interacting with Code Quality report
  target: No defined goal currently
  org: Sec Section
  section: sec
  stage: secure
  group: static_analysis
  public: true
  pi_type: CatMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Transition work still needs to be completed to have Code Quality usage roll into Static Analysis fully. This graph is a shortcut until that work is completed. [Tracking issue](https://gitlab.com/gitlab-org/gitlab/-/issues/330649#note_644514623) 
  implementation:
    status: Instrumentation
    reasons:
    - Code Quality is not currently reporting with Static Analysis GMAU as usage ping still needs to be updated. 
  lessons:
    learned:
      -  Code Quality is used by a small number of passionate customers. 
      - We're hearing market feedback that customers find lower value in code quality results compared to security reports. Customers are actively moving to tools that provide more separation between code quality and security reports. 
  monthly_focus:
    goals:
      -  Continue the handoff of Code Quality from Verify. 
  metric_name: CatMAU, static analysis - all CatMAU
  sisense_data:
    chart: 12433011
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/330649

- name: Secure:Static Analysis - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 2
    reasons:
    - Error budgets as a concept has rapidly matured and now has [much more information](https://about.gitlab.com/handbook/engineering/error-budgets/#budget-spend-by-stage-group) about what our goals are with this metric which has helped the team navigate this new PI metric.
  implementation:
    status: Complete
  lessons:
    learned:
      - We've dug deep into this data and have [identified code quality report API](https://gitlab.com/gitlab-org/gitlab/-/issues/337994) as the major cause of our error budget spend. We'll be investigating improvements to this, but code quality is a very low MAU so this is not a big impact to our user base at all. Currently, SA does not have adequate resources to cover all our categories and Code Quality and Secret detection are currently not in active development by the SA team.
  monthly_focus:
    goals:
      - We are focused on potential improvements to our error budget to resolve the code quality API endpoint performance issues which falls in line with our existing performance and scaling work. [Tracking issue](https://gitlab.com/gitlab-org/gitlab/-/issues/337994)
  sisense_data:
    chart: 12191181
    dashboard: 892459
    embed: v2
  sisense_data_secondary:
    chart: 12191183
    dashboard: 892459
    embed: v2

- name: Secure:Dynamic Analysis - GMAU - Users running a Dynamic Analysis scan
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: Number of unique users who have run one or more DAST or Fuzzing jobs.
  target: Progressively increasing month-over-month, >10% per-month
  org: Sec Section
  section: sec
  stage: secure
  group: dynamic_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track - reliable SaaS data on PM-managed charts shows overall usage continues to trend upwards. See tertiary chart.
  implementation:
    status: Complete
    reasons:
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
  lessons:
    learned:
      - Major difference in DAST usage between PM charts and Data team charts. Data team charts also show paid usage well above overall usage, which is impossible. Investigation is needed into Data team metrics to understand why their charts are so different than the raw data analysis.
      - Data team charts only include DAST, not API or Coverage-guided fuzzing, so the only source of that data is the PM-managed chart.
      - DAST visibility was boosted by being a headliner in the 13.12 release post. Most of the direct growth can be contributed to the increased visibility and better month over month user retention.
  monthly_focus:
    goals:
      - Work on database reliability and scalability issues
      - Browser-based DAST scanning - enterprise readiness focus on crawling, authentication, and vulnerability checks
      - Scalability and enterprise readiness work on API Security scanner
      - Finish work related to on-demand DAST - scan scheduling and additional site validation options
  metric_name: user_dast_jobs
  sisense_data:
    chart: 9980137
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 9980139
    dashboard: 758607
    embed: v2
  sisense_data_tertiary:
    chart: 12408922
    dashboard: 703762
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Dynamic Analysis - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-dynamic_analysis/stage-groups-group-dashboard-secure-dynamic-analysis)
  sisense_data:
    chart: 12191204
    dashboard: 892459
    embed: v2
  sisense_data_secondary:
    chart: 12196879
    dashboard: 892459
    embed: v2

- name: Secure:Composition Analysis - GMAU - Users running any SCA scanners
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The highest of the number of unique users who have run one or more Dependency Scanning jobs, or number of unique users who have run one or more License Scanning jobs.
  target: 2% higher than current month (SaaS and self-managed combined) for All and Paid
  org: Sec Section
  section: sec
  stage: secure
  group: composition_analysis
  public: true
  pi_type: GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track
  implementation:
    status: Complete
    reasons:
    - Currently our data is a lagging indicator (the majority of our users are self hosted and update 3 months behind current), trends are accurate but due to extrapolation of self-hosted, low opt-in of self-hosted, overall missing granularity, and 28 day cycles actual numbers are imprecise.
    - PM is utilizing her [own charts](https://app.periscopedata.com/app/gitlab/749790/Secure-SCA---PI---Software-Composition-Analysis---Schwartz) for now due to distrust in provided charts.
  lessons:
    learned:
      - Nothing through use of xMAU Metrics.
  monthly_focus:
    goals:
      - Performance, Reliability, Technical Debt, Scaleability and Maintainability work for Q3, after coming off the database rapid action prior.
  metric_name: user_license_management_jobs, user_dependency_scanning_jobs
  sisense_data:
    chart: 9967897
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10102606
    dashboard: 758607
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Composition Analysis - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 3
    reasons:
    - We are meeting budget, however we are going to continue to look for improvements as we are running very close to target.
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-composition_analysis/stage-groups-group-dashboard-secure-composition-analysis)
  sisense_data:
    chart: 12196882
    dashboard: 892459
    embed: v2
  sisense_data_secondary:
    chart: 12196885
    dashboard: 892459
    embed: v2

- name: Secure:Threat Insights - GMAU - Users interacting with Secure UI
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: Number of unique sessions viewing a security dashboard, pipeline security
    report, or expanding MR security report. Same as Paid until OSS projects
    can be separated.
  target: Progressively increasing month-over-month, >2% per-month
  org: Sec Section
  section: sec
  stage: secure
  group: threat_insights
  public: true
  pi_type: GMAU
  product_analytics_type: SaaS
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Steady upward trend of usage
  implementation:
    status: Definition
    reasons:
    - Collection is complete for .com only
    - Snowplow data is session only, not unique users
    - Can't currently differentiate between free OSS Gold/Ultimate users and paid Gold/Ultimate users.
    - Cannot separate out GitLab employee usage from customers
  lessons:
    learned:
      - Even with June and July data challenges, GMAU increased significantly
      - Spike is primarly concentrated on Security Center pages
      - Looking at referring URLs, uptick seems likely due to "More" menu redesign on 2021-06-14
      - Expect to see traffic level off and even return to previous levels as users finish exploring the redesign and realize it was a cosmetic change only
  monthly_focus:
    goals:
      - Focus on Rapid Actions and key technical debt around performance, scalability, and maintainability
      - Continue to work on [Error Budget overages](https://about.gitlab.com/handbook/product/sec-section-performance-indicators/#securethreat-insights---error-budget-for-gitlabcom) and work to address
  metric_name: n/a
  sisense_data:
    chart: 9244137
    dashboard: 707777
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/5670

- name: Secure:Threat Insights - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-threat_insights/stage-groups-group-dashboard-secure-threat-insights)
    - RCA for [Error budget and remediation](https://gitlab.com/gitlab-org/gitlab/-/issues/329414)
  sisense_data:
    chart: 12196887
    dashboard: 892459
    embed: v2
  sisense_data_secondary:
    chart: 12196889
    dashboard: 892459
    embed: v2

- name: Protect:Container Security - SMAU, GMAU - Users running Container Scanning or interacting with Protect UI
  base_path: "/handbook/product/sec-section-performance-indicators/"
  definition: The number of users who have run a container scanning job or interacted with the Protect UI in the last 28 days of the month.
  target: Progressively increasing month-over-month, >2%
  org: Sec Section
  section: sec
  stage: Protect
  group: container_security
  public: true
  pi_type: SMAU, GMAU
  product_analytics_type: Both
  plan_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Usage decreased some in June and mostly flatlined in July.  This temporary decline in usage was expected and anticipated as customers adapt their pipelines to work with the new Trivy scanner.  Long-term, this change is expected to provide a significant boost to usage.
  implementation:
    status: Complete
    reasons:
    - Limitations in data (see SCA limitations above)
  lessons:
    learned:
      - The switch from Clair to Trivy has allowed us to close 20+ outstanding customer issues and bugs.  Adoption is expected to increase significantly as users migrate to the new scanner over time.
  monthly_focus:
    goals:
      - Allowing users to scan container images in a running/production Kubernetes environment (https://gitlab.com/groups/gitlab-org/-/epics/3410)
      - Allow Users to Edit yaml-mode Scan Execution Policies in the Policy UI (https://gitlab.com/groups/gitlab-org/-/epics/5362) is being worked on for an August release.  This change will allow us to default the policy feature flag to on.
  metric_name: user_container_scanning_jobs
  sisense_data:
    chart: 10039269
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039569
    dashboard: 758607
    embed: v2

- name: Protect:Container Security  - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - The majority of our budget is from parsing of our artifacts which tends to take more than a 1 second, which is the default value for error budgets.
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-container_security/stage-groups-group-dashboard-protect-container-security)
  sisense_data:
    chart: 12196932
    dashboard: 892737
    embed: v2
  sisense_data_secondary:
    chart: 12196940
    dashboard: 892737
    embed: v2
    
